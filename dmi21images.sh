#!/bin/bash
# Download 100 5G themed images from each host name in a list

# Dependencies:
# sudo apt install tor wget parallel rpl csvkit

# Check for dependencies and install them if necessary
echo Checking dependencies...
for DEP in tor wget parallel rpl; do
    if ! which $DEP ; then sudo apt install $DEP ; fi
done
# This is an exception: different package name and the executable name...
if ! which csvcut ; then sudo apt install csvkit ; fi

# Configure Tor to give new IP every time we run a command through it
sudo rpl -q "IsolatePID 0" "IsolatePID 1" /etc/tor/torsocks.conf
sudo rpl -q "#IsolatePID 1" "IsolatePID 1" /etc/tor/torsocks.conf

# Input a CSV file which has a "URL" column
INPUT="urls.csv"

# Extract the URLs from the URL column into a text file and take care not to write the header
csvcut -c URL $INPUT | grep -v URL > vendor_urls.txt

# remove https://, http:// and final slash (|) from URLs
sed -e 's|https://||' -e 's|http://||' -e 's|/$||' -i vendor_urls.txt

# Qwant search engine image search API query
QUERY="https://api.qwant.com/api/search/images?count=100&offset=0&q=%22site:{}%20+5G%22&t=images&uiv=1"

# Run QUERY for each URL through Tor (check /etc/tor/torrc settings
mkdir jsons
parallel -t -j 1 --bar --joblog searchresults_job.log torify wget -O jsons/{}.json \"$QUERY\" :::: vendor_urls.txt

# Make sure the image_urls.txt file is empty
echo -n > image_urls.txt

# Extract URLs from the json files into image_urls.txt
parallel -t --bar --joblog url_from_json_job.log jq . {} '|' grep \'\"media\"\' '|' cut -d : -f 2-3 '>>' image_urls.txt ::: jsons/*.json

# Remove empty linkes and junk characters from the beginning and end of the lines
sed -e '/^$/d' -e 's/^ \"//' -e  's/\",$//' -i image_urls.txt

# Download images into a new folder
rm -rf images
mkdir images
cd images
parallel -t --bar --joblog images_job.log wget --quiet --timeout=6 {} :::: ../image_urls.txt
cd ..

echo READY

